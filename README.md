# Portfolio
__Thomas Robert's Portfolio__

## Setup
Make sur [Nodejs][] and [npm][] is installed
```
git clone git@bitbucket.org:Satche/portfolio.git <name>
cd <name>
npm install
npm start
```

## Usage
1. Run `npm start`. It will start a local server on http://localhost:3000
2. Do your modifications in `src` folder
3. Run `npm run production` to compile a production file available in `dist` folder

## Contributing
You can contribute in two differents ways:

1. Init the project (read *Setup* section) and edit/add the files you want in `src` folder, gulp will auto-compile pug and sass files for you.
2. You can just add files in the `dist` folder. Please, name your files with "custom" to avoid compilation issues.

## Built With
* HTML5, CSS3, Javascript
* [jQuery][], [Bootstrap][]
* [Pug][], [Sass][]
* [Nodejs][], [Gulp][]
* [Atom][]
* [Git][]

## Authors
* **Thomas Robert** - *Web design, front-end development* - [Github](https://github.com/Satche), [Bitbucket](https://bitbucket.org/Satche/), [Website](http://satche.ch)

*Also see [humans.txt](humans.txt)*

[//]: # ( LINKS REFERENCES )

[jQuery]: https://jquery.com/ "jQuery: JS library"
[Bootstrap]: http://getbootstrap.com "Bootstrap: CSS Framework"
[Pug]: https://pugjs.org "Pug: HTML Pre-processor"
[Sass]: http://sass-lang.com/ "Sass: CSS Pre-processor"
[Nodejs]: https://nodejs.org "Nodejs: JS runtime platform"
[npm]: https://www.npmjs.com/ "Nodejs Package Manager"
[Gulp]: http://gulpjs.com/ "Gulpjs: Tasker"
[Atom]: atom.io "Atom: Text editor"
[Git]: https://git-scm.com/ "Git: Versioning"

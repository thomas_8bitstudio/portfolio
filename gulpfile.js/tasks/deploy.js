var config = require('../config')
var ghPages = require('gulp-gh-pages')
var inquirer = require('inquirer')
var gulp = require('gulp')
var open = require('open')
var os = require('os')
var package = require('../../package.json')
var path = require('path')

var settings = {
  url: package.homepage,
  src: path.join(config.root.dest, '/**/*'),
  ghPages: {
    cacheDir: path.join(os.tmpdir(), package.name),
    remoteUrl: package.repository.url,
    branch: 'master',
    message: ''
  }
}

var questions = {
  type: 'input',
  name: 'commit',
  message: 'What\'s your first name'
};

gulp.task('ghPages', function() {
  return gulp.src(settings.src)
    .pipe(ghPages(settings.ghPages))
    .on('end', function() {
      open(settings.url)
    })
});

var deployTask = function(done) {
  inquirer.prompt(questions).then(function(answer) {
    settings.ghPages.message = answer.commit;
    gulp.start('ghPages');
    done();
  });
}

gulp.task('deploy', ['production'], deployTask);

module.exports = deployTask

var config        = require('./gulpfile.js/config')
var karmaWebpack  = require('karma-webpack')
var webpackConfig = require('./gulpfile.js/lib/webpack-multi-config')
var path          = require('path')

var testSrc = path.join(config.root.src, config.tasks.js.src, '/**/__tests__/*')

var karmaConfig = {
  frameworks: [],
  files: [ testSrc ],
  preprocessors: {},
  webpack: webpackConfig('test'),
  reporters: ['nyan'],
  browsers: ['Firefox','Chrome','Safari']
}

karmaConfig.preprocessors[testSrc] = ['webpack']

module.exports = function(config) {
  config.set(karmaConfig)
}

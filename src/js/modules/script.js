import $ from 'jquery'

// Smooth scroll
$('a[href*="#"]:not([href="#"])').click(function() {
  $('html, body').animate({
    scrollTop: $($(this).attr('href')).offset().top
  }, 600);
  return false;
});

// displayOnScroll
function onScroll(event) {
  $('[display="false"]').each(function() {
    if ($(document).scrollTop() >= $(this).offset().top - $(window).height() * 0.8) {
      if ($(window).width() > 768) {
        var i = $(this).attr("delay");
      } else {
        var i = 0;
      }

      $(this).delay(i).queue(function(next) {
        $(this).attr("display", "true");
        next();
      });
    }
  });
}
$(document).on("scroll", onScroll);

// Scrollspy
$(function() {
  var sections = {};
  var i = 0;

  Array.prototype.forEach.call($("section"), function(e) {
    sections[e.id] = e.offsetTop;
  });
  window.onscroll = function() {
    var scrollPosition = $(document).scrollTop() || $("body").scrollTop();
    for (i in sections) {
      if (sections[i] <= scrollPosition + 10) {
        $('.active').removeClass('active');
        $('a[href*=' + i + ']').addClass('active');
      }
    }
  };
});

// Navbar hidden
$(function() {
  $('nav ul, #offset').click(function() {
    $('nav').toggleClass('navbar_show');
  });
});
